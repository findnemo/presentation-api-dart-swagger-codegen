import 'package:openapi/api.dart';
import 'package:test/test.dart';

// tests for PickupDropoffResponse
void main() {
    var instance = new PickupDropoffResponse();

  group('test PickupDropoffResponse', () {
    // num id (default value: null)
    test('to test the property `id`', () async {
      // TODO
    });

    // num driverId (default value: null)
    test('to test the property `driverId`', () async {
      // TODO
    });

    // num tripId (default value: null)
    test('to test the property `tripId`', () async {
      // TODO
    });

    // num childId (default value: null)
    test('to test the property `childId`', () async {
      // TODO
    });

    // String pickupTime (default value: null)
    test('to test the property `pickupTime`', () async {
      // TODO
    });

    // String dropoffTime (default value: null)
    test('to test the property `dropoffTime`', () async {
      // TODO
    });

    // String createdAt (default value: null)
    test('to test the property `createdAt`', () async {
      // TODO
    });

    // String updatedAt (default value: null)
    test('to test the property `updatedAt`', () async {
      // TODO
    });


  });

}
