import 'package:openapi/api.dart';
import 'package:test/test.dart';


/// tests for ChildGroupApi
void main() {
  var instance = ChildGroupApi();

  group('tests for ChildGroupApi', () {
    // Returns added child to the group
    //
    // Add childs to group
    //
    //Future<ChildGroupResponse> groupApiChildsGroupsGroupIdAddPost(int groupId, ChildGroupChildIdsBody childGroupChildIdsBody) async 
    test('test groupApiChildsGroupsGroupIdAddPost', () async {
      // TODO
    });

    // Returns number of child removed from the group
    //
    // Removes childs from group
    //
    //Future groupApiChildsGroupsGroupIdDeletePut(int groupId, ChildGroupChildIdsBody childGroupChildIdsBody) async 
    test('test groupApiChildsGroupsGroupIdDeletePut', () async {
      // TODO
    });

    // Returns array of child-group by groupId
    //
    // Get childs by groupId
    //
    //Future<ChildGroupWithChildWithGroupResponse> groupApiChildsGroupsGroupIdGet(int groupId) async 
    test('test groupApiChildsGroupsGroupIdGet', () async {
      // TODO
    });

  });
}
