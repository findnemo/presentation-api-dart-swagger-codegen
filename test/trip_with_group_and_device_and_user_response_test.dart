import 'package:openapi/api.dart';
import 'package:test/test.dart';

// tests for TripWithGroupAndDeviceAndUserResponse
void main() {
    var instance = new TripWithGroupAndDeviceAndUserResponse();

  group('test TripWithGroupAndDeviceAndUserResponse', () {
    // num id (default value: null)
    test('to test the property `id`', () async {
      // TODO
    });

    // num driverId (default value: null)
    test('to test the property `driverId`', () async {
      // TODO
    });

    // num groupId (default value: null)
    test('to test the property `groupId`', () async {
      // TODO
    });

    // num deviceId (default value: null)
    test('to test the property `deviceId`', () async {
      // TODO
    });

    // String startTime (default value: null)
    test('to test the property `startTime`', () async {
      // TODO
    });

    // String endTime (default value: null)
    test('to test the property `endTime`', () async {
      // TODO
    });

    // UserResponse driver (default value: null)
    test('to test the property `driver`', () async {
      // TODO
    });

    // GroupResponse group (default value: null)
    test('to test the property `group`', () async {
      // TODO
    });

    // DeviceResponse device (default value: null)
    test('to test the property `device`', () async {
      // TODO
    });

    // String createdAt (default value: null)
    test('to test the property `createdAt`', () async {
      // TODO
    });

    // String updatedAt (default value: null)
    test('to test the property `updatedAt`', () async {
      // TODO
    });


  });

}
