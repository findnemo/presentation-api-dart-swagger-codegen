import 'package:openapi/api.dart';
import 'package:test/test.dart';

// tests for UserTypeWithUserWithAddressResponse
void main() {
    var instance = new UserTypeWithUserWithAddressResponse();

  group('test UserTypeWithUserWithAddressResponse', () {
    // num id (default value: null)
    test('to test the property `id`', () async {
      // TODO
    });

    // num userId (default value: null)
    test('to test the property `userId`', () async {
      // TODO
    });

    // bool isDriver (default value: null)
    test('to test the property `isDriver`', () async {
      // TODO
    });

    // bool isParent (default value: null)
    test('to test the property `isParent`', () async {
      // TODO
    });

    // UserWithAddressResponse user (default value: null)
    test('to test the property `user`', () async {
      // TODO
    });

    // String updatedAt (default value: null)
    test('to test the property `updatedAt`', () async {
      // TODO
    });

    // String createdAt (default value: null)
    test('to test the property `createdAt`', () async {
      // TODO
    });


  });

}
