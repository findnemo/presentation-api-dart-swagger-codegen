import 'package:openapi/api.dart';
import 'package:test/test.dart';


/// tests for GroupApi
void main() {
  var instance = GroupApi();

  group('tests for GroupApi', () {
    // Returns number of groups deleted
    //
    // Delete group
    //
    //Future groupApiGroupIdDelete(int id) async 
    test('test groupApiGroupIdDelete', () async {
      // TODO
    });

    // Returns newly created group
    //
    // Create group
    //
    //Future<GroupResponse> groupApiGroupPost(CreateGroupBody createGroupBody) async 
    test('test groupApiGroupPost', () async {
      // TODO
    });

    // Returns updated group
    //
    // Update group
    //
    //Future groupApiGroupPut(UpdateGroupBody updateGroupBody) async 
    test('test groupApiGroupPut', () async {
      // TODO
    });

  });
}
