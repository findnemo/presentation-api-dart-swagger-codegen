import 'package:openapi/api.dart';
import 'package:test/test.dart';

// tests for ChildResponse
void main() {
    var instance = new ChildResponse();

  group('test ChildResponse', () {
    // num id (default value: null)
    test('to test the property `id`', () async {
      // TODO
    });

    // String fullName (default value: null)
    test('to test the property `fullName`', () async {
      // TODO
    });

    // num parentId (default value: null)
    test('to test the property `parentId`', () async {
      // TODO
    });

    // num addressId (default value: null)
    test('to test the property `addressId`', () async {
      // TODO
    });

    // String createdAt (default value: null)
    test('to test the property `createdAt`', () async {
      // TODO
    });

    // String updatedAt (default value: null)
    test('to test the property `updatedAt`', () async {
      // TODO
    });

    // UserResponse parent (default value: null)
    test('to test the property `parent`', () async {
      // TODO
    });

    // AddressResponse address (default value: null)
    test('to test the property `address`', () async {
      // TODO
    });


  });

}
