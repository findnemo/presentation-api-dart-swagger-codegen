import 'package:openapi/api.dart';
import 'package:test/test.dart';

// tests for DriverGroupWithGroupAndUserResponse
void main() {
    var instance = new DriverGroupWithGroupAndUserResponse();

  group('test DriverGroupWithGroupAndUserResponse', () {
    // num driverId (default value: null)
    test('to test the property `driverId`', () async {
      // TODO
    });

    // num groupId (default value: null)
    test('to test the property `groupId`', () async {
      // TODO
    });

    // UserResponse driver (default value: null)
    test('to test the property `driver`', () async {
      // TODO
    });

    // GroupResponse group (default value: null)
    test('to test the property `group`', () async {
      // TODO
    });

    // String permission (default value: null)
    test('to test the property `permission`', () async {
      // TODO
    });

    // String createdAt (default value: null)
    test('to test the property `createdAt`', () async {
      // TODO
    });

    // String updatedAt (default value: null)
    test('to test the property `updatedAt`', () async {
      // TODO
    });


  });

}
