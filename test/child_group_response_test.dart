import 'package:openapi/api.dart';
import 'package:test/test.dart';

// tests for ChildGroupResponse
void main() {
    var instance = new ChildGroupResponse();

  group('test ChildGroupResponse', () {
    // num childId (default value: null)
    test('to test the property `childId`', () async {
      // TODO
    });

    // num groupId (default value: null)
    test('to test the property `groupId`', () async {
      // TODO
    });

    // ChildResponse child (default value: null)
    test('to test the property `child`', () async {
      // TODO
    });

    // GroupResponse group (default value: null)
    test('to test the property `group`', () async {
      // TODO
    });

    // String createdAt (default value: null)
    test('to test the property `createdAt`', () async {
      // TODO
    });

    // String updatedAt (default value: null)
    test('to test the property `updatedAt`', () async {
      // TODO
    });


  });

}
