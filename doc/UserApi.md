# openapi.api.UserApi

## Load the API package
```dart
import 'package:openapi/api.dart';
```

All URIs are relative to *https://production.presentation-api.api.findnemo.in*

Method | HTTP request | Description
------------- | ------------- | -------------
[**userApiUserDelete**](UserApi.md#userApiUserDelete) | **DELETE** /user-api/user/ | Returns number of user deleted by phone number
[**userApiUserGet**](UserApi.md#userApiUserGet) | **GET** /user-api/user/ | Returns user object by phone number
[**userApiUserPost**](UserApi.md#userApiUserPost) | **POST** /user-api/user/ | Returns user object
[**userApiUserPut**](UserApi.md#userApiUserPut) | **PUT** /user-api/user/ | Returns number of user updated


# **userApiUserDelete**
> DeleteRecordsResponse userApiUserDelete()

Returns number of user deleted by phone number

Delete the user by phone number

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BearerAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BearerAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BearerAuth').password = 'YOUR_PASSWORD';

var api_instance = UserApi();

try { 
    var result = api_instance.userApiUserDelete();
    print(result);
} catch (e) {
    print("Exception when calling UserApi->userApiUserDelete: $e\n");
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**DeleteRecordsResponse**](DeleteRecordsResponse.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **userApiUserGet**
> UserWithAddressResponse userApiUserGet()

Returns user object by phone number

Get the user by phone number

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BearerAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BearerAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BearerAuth').password = 'YOUR_PASSWORD';

var api_instance = UserApi();

try { 
    var result = api_instance.userApiUserGet();
    print(result);
} catch (e) {
    print("Exception when calling UserApi->userApiUserGet: $e\n");
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**UserWithAddressResponse**](UserWithAddressResponse.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **userApiUserPost**
> UserResponse userApiUserPost(createUserWithAddressBody)

Returns user object

Create a user as driver and/or parent

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BearerAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BearerAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BearerAuth').password = 'YOUR_PASSWORD';

var api_instance = UserApi();
var createUserWithAddressBody = CreateUserWithAddressBody(); // CreateUserWithAddressBody | User body with address

try { 
    var result = api_instance.userApiUserPost(createUserWithAddressBody);
    print(result);
} catch (e) {
    print("Exception when calling UserApi->userApiUserPost: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **createUserWithAddressBody** | [**CreateUserWithAddressBody**](CreateUserWithAddressBody.md)| User body with address | 

### Return type

[**UserResponse**](UserResponse.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **userApiUserPut**
> UpdateRecordsResponse userApiUserPut(updateUserWithAddressBody)

Returns number of user updated

Update a user as driver and/or parent

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BearerAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BearerAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BearerAuth').password = 'YOUR_PASSWORD';

var api_instance = UserApi();
var updateUserWithAddressBody = UpdateUserWithAddressBody(); // UpdateUserWithAddressBody | User body with address

try { 
    var result = api_instance.userApiUserPut(updateUserWithAddressBody);
    print(result);
} catch (e) {
    print("Exception when calling UserApi->userApiUserPut: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **updateUserWithAddressBody** | [**UpdateUserWithAddressBody**](UpdateUserWithAddressBody.md)| User body with address | 

### Return type

[**UpdateRecordsResponse**](UpdateRecordsResponse.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

