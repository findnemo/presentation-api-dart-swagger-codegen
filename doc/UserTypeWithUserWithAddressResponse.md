# openapi.model.UserTypeWithUserWithAddressResponse

## Load the model package
```dart
import 'package:openapi/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **num** |  | [default to null]
**userId** | **num** |  | [default to null]
**isDriver** | **bool** |  | [default to null]
**isParent** | **bool** |  | [default to null]
**user** | [**UserWithAddressResponse**](UserWithAddressResponse.md) |  | [default to null]
**updatedAt** | **String** |  | [default to null]
**createdAt** | **String** |  | [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


