# openapi.model.DeviceWithUserResponse

## Load the model package
```dart
import 'package:openapi/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **num** |  | [default to null]
**uId** | **String** |  | [default to null]
**model** | **String** |  | [optional] [default to null]
**make** | **String** |  | [optional] [default to null]
**userId** | **num** |  | [default to null]
**fcmToken** | **String** |  | [optional] [default to null]
**user** | [**UserResponse**](UserResponse.md) |  | [default to null]
**updatedAt** | **String** |  | [optional] [default to null]
**createdAt** | **String** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


