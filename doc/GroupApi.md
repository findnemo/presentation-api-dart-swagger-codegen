# openapi.api.GroupApi

## Load the API package
```dart
import 'package:openapi/api.dart';
```

All URIs are relative to *https://production.presentation-api.api.findnemo.in*

Method | HTTP request | Description
------------- | ------------- | -------------
[**groupApiGroupIdDelete**](GroupApi.md#groupApiGroupIdDelete) | **DELETE** /group-api/group/{id}/ | Returns number of groups deleted
[**groupApiGroupPost**](GroupApi.md#groupApiGroupPost) | **POST** /group-api/group/ | Returns newly created group
[**groupApiGroupPut**](GroupApi.md#groupApiGroupPut) | **PUT** /group-api/group/ | Returns updated group


# **groupApiGroupIdDelete**
> DeleteRecordsResponse groupApiGroupIdDelete(id)

Returns number of groups deleted

Delete group

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BearerAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BearerAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BearerAuth').password = 'YOUR_PASSWORD';

var api_instance = GroupApi();
var id = 56; // int | Group id

try { 
    var result = api_instance.groupApiGroupIdDelete(id);
    print(result);
} catch (e) {
    print("Exception when calling GroupApi->groupApiGroupIdDelete: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Group id | [default to null]

### Return type

[**DeleteRecordsResponse**](DeleteRecordsResponse.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **groupApiGroupPost**
> GroupResponse groupApiGroupPost(createGroupBody)

Returns newly created group

Create group

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BearerAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BearerAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BearerAuth').password = 'YOUR_PASSWORD';

var api_instance = GroupApi();
var createGroupBody = CreateGroupBody(); // CreateGroupBody | Group body

try { 
    var result = api_instance.groupApiGroupPost(createGroupBody);
    print(result);
} catch (e) {
    print("Exception when calling GroupApi->groupApiGroupPost: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **createGroupBody** | [**CreateGroupBody**](CreateGroupBody.md)| Group body | 

### Return type

[**GroupResponse**](GroupResponse.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **groupApiGroupPut**
> UpdateRecordsResponse groupApiGroupPut(updateGroupBody)

Returns updated group

Update group

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BearerAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BearerAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BearerAuth').password = 'YOUR_PASSWORD';

var api_instance = GroupApi();
var updateGroupBody = UpdateGroupBody(); // UpdateGroupBody | Group body

try { 
    var result = api_instance.groupApiGroupPut(updateGroupBody);
    print(result);
} catch (e) {
    print("Exception when calling GroupApi->groupApiGroupPut: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **updateGroupBody** | [**UpdateGroupBody**](UpdateGroupBody.md)| Group body | 

### Return type

[**UpdateRecordsResponse**](UpdateRecordsResponse.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

