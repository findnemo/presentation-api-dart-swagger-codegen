# openapi.api.DeviceApi

## Load the API package
```dart
import 'package:openapi/api.dart';
```

All URIs are relative to *https://production.presentation-api.api.findnemo.in*

Method | HTTP request | Description
------------- | ------------- | -------------
[**deviceApiDevicePost**](DeviceApi.md#deviceApiDevicePost) | **POST** /device-api/device/ | Returns device object
[**deviceApiDeviceUIdUIdGet**](DeviceApi.md#deviceApiDeviceUIdUIdGet) | **GET** /device-api/device/uId/{uId}/ | Returns device object


# **deviceApiDevicePost**
> DeviceResponse deviceApiDevicePost(createDeviceBody)

Returns device object

Create a device for a user

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BearerAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BearerAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BearerAuth').password = 'YOUR_PASSWORD';

var api_instance = DeviceApi();
var createDeviceBody = CreateDeviceBody(); // CreateDeviceBody | Device body

try { 
    var result = api_instance.deviceApiDevicePost(createDeviceBody);
    print(result);
} catch (e) {
    print("Exception when calling DeviceApi->deviceApiDevicePost: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **createDeviceBody** | [**CreateDeviceBody**](CreateDeviceBody.md)| Device body | 

### Return type

[**DeviceResponse**](DeviceResponse.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **deviceApiDeviceUIdUIdGet**
> DeviceWithUserResponse deviceApiDeviceUIdUIdGet(uId)

Returns device object

Get a device for a user by uId

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BearerAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BearerAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BearerAuth').password = 'YOUR_PASSWORD';

var api_instance = DeviceApi();
var uId = uId_example; // String | Device uId

try { 
    var result = api_instance.deviceApiDeviceUIdUIdGet(uId);
    print(result);
} catch (e) {
    print("Exception when calling DeviceApi->deviceApiDeviceUIdUIdGet: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **uId** | **String**| Device uId | [default to null]

### Return type

[**DeviceWithUserResponse**](DeviceWithUserResponse.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

