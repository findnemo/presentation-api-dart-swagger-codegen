# openapi.api.PickupDropoffApi

## Load the API package
```dart
import 'package:openapi/api.dart';
```

All URIs are relative to *https://production.presentation-api.api.findnemo.in*

Method | HTTP request | Description
------------- | ------------- | -------------
[**groupApiPickupDropoffsTripsTripIdGet**](PickupDropoffApi.md#groupApiPickupDropoffsTripsTripIdGet) | **GET** /group-api/pickup-dropoffs/trips/{tripId}/ | Returns array of pickup-dropoff by tripId
[**groupApiTripsDropoffPut**](PickupDropoffApi.md#groupApiTripsDropoffPut) | **PUT** /group-api/trips/dropoff/ | Returns number of children Dropoff
[**groupApiTripsPickupPost**](PickupDropoffApi.md#groupApiTripsPickupPost) | **POST** /group-api/trips/pickup/ | Returns array of pickupdropoff


# **groupApiPickupDropoffsTripsTripIdGet**
> List<PickupDropoffWithTripAndChildAndDriverResponse> groupApiPickupDropoffsTripsTripIdGet(tripId)

Returns array of pickup-dropoff by tripId

Get pickup-dropoff by tripId

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BearerAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BearerAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BearerAuth').password = 'YOUR_PASSWORD';

var api_instance = PickupDropoffApi();
var tripId = 56; // int | Trip id

try { 
    var result = api_instance.groupApiPickupDropoffsTripsTripIdGet(tripId);
    print(result);
} catch (e) {
    print("Exception when calling PickupDropoffApi->groupApiPickupDropoffsTripsTripIdGet: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tripId** | **int**| Trip id | [default to null]

### Return type

[**List<PickupDropoffWithTripAndChildAndDriverResponse>**](PickupDropoffWithTripAndChildAndDriverResponse.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **groupApiTripsDropoffPut**
> UpdateRecordsResponse groupApiTripsDropoffPut(createDropoffBody)

Returns number of children Dropoff

Dropoff childrens by tripId

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BearerAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BearerAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BearerAuth').password = 'YOUR_PASSWORD';

var api_instance = PickupDropoffApi();
var createDropoffBody = CreateDropoffBody(); // CreateDropoffBody | Group body

try { 
    var result = api_instance.groupApiTripsDropoffPut(createDropoffBody);
    print(result);
} catch (e) {
    print("Exception when calling PickupDropoffApi->groupApiTripsDropoffPut: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **createDropoffBody** | [**CreateDropoffBody**](CreateDropoffBody.md)| Group body | 

### Return type

[**UpdateRecordsResponse**](UpdateRecordsResponse.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **groupApiTripsPickupPost**
> List<PickupDropoffResponse> groupApiTripsPickupPost(createPickupBody)

Returns array of pickupdropoff

Pickup childrens by tripId

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BearerAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BearerAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BearerAuth').password = 'YOUR_PASSWORD';

var api_instance = PickupDropoffApi();
var createPickupBody = CreatePickupBody(); // CreatePickupBody | Group body

try { 
    var result = api_instance.groupApiTripsPickupPost(createPickupBody);
    print(result);
} catch (e) {
    print("Exception when calling PickupDropoffApi->groupApiTripsPickupPost: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **createPickupBody** | [**CreatePickupBody**](CreatePickupBody.md)| Group body | 

### Return type

[**List<PickupDropoffResponse>**](PickupDropoffResponse.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

