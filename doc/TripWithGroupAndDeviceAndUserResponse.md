# openapi.model.TripWithGroupAndDeviceAndUserResponse

## Load the model package
```dart
import 'package:openapi/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **num** |  | [default to null]
**driverId** | **num** |  | [default to null]
**groupId** | **num** |  | [default to null]
**deviceId** | **num** |  | [default to null]
**startTime** | **String** |  | [optional] [default to null]
**endTime** | **String** |  | [optional] [default to null]
**childId** | **num** |  | [optional] [default to null]
**startLatitude** | **num** |  | [optional] [default to null]
**startLongitude** | **num** |  | [optional] [default to null]
**endLatitude** | **num** |  | [optional] [default to null]
**endLongitude** | **num** |  | [optional] [default to null]
**driver** | [**UserResponse**](UserResponse.md) |  | [default to null]
**group** | [**GroupResponse**](GroupResponse.md) |  | [default to null]
**device** | [**DeviceResponse**](DeviceResponse.md) |  | [default to null]
**createdAt** | **String** |  | [default to null]
**updatedAt** | **String** |  | [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


