# openapi.model.DriverGroupResponse

## Load the model package
```dart
import 'package:openapi/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**driverId** | **num** |  | [default to null]
**groupId** | **num** |  | [default to null]
**permission** | **String** |  | [default to null]
**createdAt** | **String** |  | [default to null]
**updatedAt** | **String** |  | [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


