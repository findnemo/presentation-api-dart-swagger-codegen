# openapi.model.UpdateUserWithAddressBody

## Load the model package
```dart
import 'package:openapi/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **num** |  | [default to null]
**fullName** | **String** |  | [default to null]
**profileImageUrl** | **String** |  | [optional] [default to null]
**photoIdUrl** | **String** |  | [optional] [default to null]
**email** | **String** |  | [optional] [default to null]
**dateOfBirth** | **String** |  | [optional] [default to null]
**addressId** | **num** |  | [optional] [default to null]
**address** | [**CreateAddressBody**](CreateAddressBody.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


