# openapi.model.StartTripBody

## Load the model package
```dart
import 'package:openapi/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**groupId** | **num** |  | [default to null]
**uId** | **String** |  | [default to null]
**startLatitude** | **num** |  | [default to null]
**startLongitude** | **num** |  | [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


