# openapi.api.DriverGroupApi

## Load the API package
```dart
import 'package:openapi/api.dart';
```

All URIs are relative to *https://production.presentation-api.api.findnemo.in*

Method | HTTP request | Description
------------- | ------------- | -------------
[**groupApiDriverGroupsGet**](DriverGroupApi.md#groupApiDriverGroupsGet) | **GET** /group-api/driver-groups/ | Returns array of group by driverId


# **groupApiDriverGroupsGet**
> List<DriverGroupWithGroupAndUserResponse> groupApiDriverGroupsGet()

Returns array of group by driverId

Get groups by driverId

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BearerAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BearerAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BearerAuth').password = 'YOUR_PASSWORD';

var api_instance = DriverGroupApi();

try { 
    var result = api_instance.groupApiDriverGroupsGet();
    print(result);
} catch (e) {
    print("Exception when calling DriverGroupApi->groupApiDriverGroupsGet: $e\n");
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**List<DriverGroupWithGroupAndUserResponse>**](DriverGroupWithGroupAndUserResponse.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

