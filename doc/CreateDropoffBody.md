# openapi.model.CreateDropoffBody

## Load the model package
```dart
import 'package:openapi/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**tripId** | **num** |  | [default to null]
**dropoffLatitude** | **num** |  | [default to null]
**dropoffLongitude** | **num** |  | [default to null]
**childIds** | **List&lt;num&gt;** |  | [default to []]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


