# openapi.model.PickupDropoffResponse

## Load the model package
```dart
import 'package:openapi/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **num** |  | [default to null]
**driverId** | **num** |  | [default to null]
**tripId** | **num** |  | [default to null]
**childId** | **num** |  | [default to null]
**pickupTime** | **String** |  | [optional] [default to null]
**dropoffTime** | **String** |  | [optional] [default to null]
**pickupLatitude** | **num** |  | [optional] [default to null]
**pickupLongitude** | **num** |  | [optional] [default to null]
**dropoffLatitude** | **num** |  | [optional] [default to null]
**dropoffLongitude** | **num** |  | [optional] [default to null]
**createdAt** | **String** |  | [default to null]
**updatedAt** | **String** |  | [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


