# openapi.api.UserTypeApi

## Load the API package
```dart
import 'package:openapi/api.dart';
```

All URIs are relative to *https://production.presentation-api.api.findnemo.in*

Method | HTTP request | Description
------------- | ------------- | -------------
[**userApiUserTypeAddRolePatch**](UserTypeApi.md#userApiUserTypeAddRolePatch) | **PATCH** /user-api/user-type/add-role/ | Returns true indicating the role has been added to the user
[**userApiUserTypeGet**](UserTypeApi.md#userApiUserTypeGet) | **GET** /user-api/user-type/ | Returns user-type object by phone number


# **userApiUserTypeAddRolePatch**
> UpdateRecordsResponse userApiUserTypeAddRolePatch(userAddRole)

Returns true indicating the role has been added to the user

Add a new role to the user

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BearerAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BearerAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BearerAuth').password = 'YOUR_PASSWORD';

var api_instance = UserTypeApi();
var userAddRole = UserAddRole(); // UserAddRole | User with role

try { 
    var result = api_instance.userApiUserTypeAddRolePatch(userAddRole);
    print(result);
} catch (e) {
    print("Exception when calling UserTypeApi->userApiUserTypeAddRolePatch: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userAddRole** | [**UserAddRole**](UserAddRole.md)| User with role | 

### Return type

[**UpdateRecordsResponse**](UpdateRecordsResponse.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **userApiUserTypeGet**
> UserTypeWithUserWithAddressResponse userApiUserTypeGet()

Returns user-type object by phone number

Get the user-type by phone number

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BearerAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BearerAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BearerAuth').password = 'YOUR_PASSWORD';

var api_instance = UserTypeApi();

try { 
    var result = api_instance.userApiUserTypeGet();
    print(result);
} catch (e) {
    print("Exception when calling UserTypeApi->userApiUserTypeGet: $e\n");
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**UserTypeWithUserWithAddressResponse**](UserTypeWithUserWithAddressResponse.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

