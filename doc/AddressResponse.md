# openapi.model.AddressResponse

## Load the model package
```dart
import 'package:openapi/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **num** |  | [default to null]
**address1** | **String** |  | [default to null]
**address2** | **String** |  | [optional] [default to null]
**address3** | **String** |  | [optional] [default to null]
**city** | **String** |  | [default to null]
**state** | **String** |  | [default to null]
**country** | **String** |  | [default to null]
**postalCode** | **String** |  | [default to null]
**createdAt** | **String** |  | [default to null]
**updatedAt** | **String** |  | [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


