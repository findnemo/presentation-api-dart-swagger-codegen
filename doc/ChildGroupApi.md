# openapi.api.ChildGroupApi

## Load the API package
```dart
import 'package:openapi/api.dart';
```

All URIs are relative to *https://production.presentation-api.api.findnemo.in*

Method | HTTP request | Description
------------- | ------------- | -------------
[**groupApiChildGroupsChildIdGet**](ChildGroupApi.md#groupApiChildGroupsChildIdGet) | **GET** /group-api/child-groups/{childId}/ | Returns array of child-group by childId
[**groupApiChildGroupsGet**](ChildGroupApi.md#groupApiChildGroupsGet) | **GET** /group-api/child-groups/ | Returns array of child-group by user
[**groupApiChildsGroupsGroupIdAddPost**](ChildGroupApi.md#groupApiChildsGroupsGroupIdAddPost) | **POST** /group-api/childs/groups/{groupId}/add/ | Returns added child to the group
[**groupApiChildsGroupsGroupIdDeletePut**](ChildGroupApi.md#groupApiChildsGroupsGroupIdDeletePut) | **PUT** /group-api/childs/groups/{groupId}/delete/ | Returns number of child removed from the group
[**groupApiChildsGroupsGroupIdGet**](ChildGroupApi.md#groupApiChildsGroupsGroupIdGet) | **GET** /group-api/childs/groups/{groupId}/ | Returns array of child-group by groupId


# **groupApiChildGroupsChildIdGet**
> List<ChildGroupWithChildWithGroupResponse> groupApiChildGroupsChildIdGet(childId)

Returns array of child-group by childId

Get childs by childId

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BearerAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BearerAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BearerAuth').password = 'YOUR_PASSWORD';

var api_instance = ChildGroupApi();
var childId = 56; // int | ChildId Id

try { 
    var result = api_instance.groupApiChildGroupsChildIdGet(childId);
    print(result);
} catch (e) {
    print("Exception when calling ChildGroupApi->groupApiChildGroupsChildIdGet: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **childId** | **int**| ChildId Id | [default to null]

### Return type

[**List<ChildGroupWithChildWithGroupResponse>**](ChildGroupWithChildWithGroupResponse.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **groupApiChildGroupsGet**
> List<ChildGroupWithChildWithGroupResponse> groupApiChildGroupsGet()

Returns array of child-group by user

Get childs by user

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BearerAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BearerAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BearerAuth').password = 'YOUR_PASSWORD';

var api_instance = ChildGroupApi();

try { 
    var result = api_instance.groupApiChildGroupsGet();
    print(result);
} catch (e) {
    print("Exception when calling ChildGroupApi->groupApiChildGroupsGet: $e\n");
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**List<ChildGroupWithChildWithGroupResponse>**](ChildGroupWithChildWithGroupResponse.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **groupApiChildsGroupsGroupIdAddPost**
> ChildGroupResponse groupApiChildsGroupsGroupIdAddPost(groupId, childGroupChildIdsBody)

Returns added child to the group

Add childs to group

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BearerAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BearerAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BearerAuth').password = 'YOUR_PASSWORD';

var api_instance = ChildGroupApi();
var groupId = 56; // int | Group Id
var childGroupChildIdsBody = ChildGroupChildIdsBody(); // ChildGroupChildIdsBody | ChildIds body

try { 
    var result = api_instance.groupApiChildsGroupsGroupIdAddPost(groupId, childGroupChildIdsBody);
    print(result);
} catch (e) {
    print("Exception when calling ChildGroupApi->groupApiChildsGroupsGroupIdAddPost: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **groupId** | **int**| Group Id | [default to null]
 **childGroupChildIdsBody** | [**ChildGroupChildIdsBody**](ChildGroupChildIdsBody.md)| ChildIds body | 

### Return type

[**ChildGroupResponse**](ChildGroupResponse.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **groupApiChildsGroupsGroupIdDeletePut**
> UpdateRecordsResponse groupApiChildsGroupsGroupIdDeletePut(groupId, childGroupChildIdsBody)

Returns number of child removed from the group

Removes childs from group

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BearerAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BearerAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BearerAuth').password = 'YOUR_PASSWORD';

var api_instance = ChildGroupApi();
var groupId = 56; // int | Group Id
var childGroupChildIdsBody = ChildGroupChildIdsBody(); // ChildGroupChildIdsBody | ChildIds body

try { 
    var result = api_instance.groupApiChildsGroupsGroupIdDeletePut(groupId, childGroupChildIdsBody);
    print(result);
} catch (e) {
    print("Exception when calling ChildGroupApi->groupApiChildsGroupsGroupIdDeletePut: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **groupId** | **int**| Group Id | [default to null]
 **childGroupChildIdsBody** | [**ChildGroupChildIdsBody**](ChildGroupChildIdsBody.md)| ChildIds body | 

### Return type

[**UpdateRecordsResponse**](UpdateRecordsResponse.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **groupApiChildsGroupsGroupIdGet**
> List<ChildGroupWithChildWithGroupResponse> groupApiChildsGroupsGroupIdGet(groupId)

Returns array of child-group by groupId

Get childs by groupId

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BearerAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BearerAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BearerAuth').password = 'YOUR_PASSWORD';

var api_instance = ChildGroupApi();
var groupId = 56; // int | Group Id

try { 
    var result = api_instance.groupApiChildsGroupsGroupIdGet(groupId);
    print(result);
} catch (e) {
    print("Exception when calling ChildGroupApi->groupApiChildsGroupsGroupIdGet: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **groupId** | **int**| Group Id | [default to null]

### Return type

[**List<ChildGroupWithChildWithGroupResponse>**](ChildGroupWithChildWithGroupResponse.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

