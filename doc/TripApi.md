# openapi.api.TripApi

## Load the API package
```dart
import 'package:openapi/api.dart';
```

All URIs are relative to *https://production.presentation-api.api.findnemo.in*

Method | HTTP request | Description
------------- | ------------- | -------------
[**groupApiChildTripsLatestGet**](TripApi.md#groupApiChildTripsLatestGet) | **GET** /group-api/child-trips/latest/ | Returns array of latest trip for each child by user
[**groupApiTripEndPut**](TripApi.md#groupApiTripEndPut) | **PUT** /group-api/trip/end/ | Return number of trips ended
[**groupApiTripStartPost**](TripApi.md#groupApiTripStartPost) | **POST** /group-api/trip/start/ | Return newly created trip
[**groupApiTripsGroupsGroupIdGet**](TripApi.md#groupApiTripsGroupsGroupIdGet) | **GET** /group-api/trips/groups/{groupId}/ | Returns array of trips by groupId


# **groupApiChildTripsLatestGet**
> List<TripWithGroupAndDeviceAndUserResponse> groupApiChildTripsLatestGet()

Returns array of latest trip for each child by user

Get latest trips for each child by user

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BearerAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BearerAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BearerAuth').password = 'YOUR_PASSWORD';

var api_instance = TripApi();

try { 
    var result = api_instance.groupApiChildTripsLatestGet();
    print(result);
} catch (e) {
    print("Exception when calling TripApi->groupApiChildTripsLatestGet: $e\n");
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**List<TripWithGroupAndDeviceAndUserResponse>**](TripWithGroupAndDeviceAndUserResponse.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **groupApiTripEndPut**
> UpdateRecordsResponse groupApiTripEndPut(endTripBody)

Return number of trips ended

End a trip

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BearerAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BearerAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BearerAuth').password = 'YOUR_PASSWORD';

var api_instance = TripApi();
var endTripBody = EndTripBody(); // EndTripBody | Group body

try { 
    var result = api_instance.groupApiTripEndPut(endTripBody);
    print(result);
} catch (e) {
    print("Exception when calling TripApi->groupApiTripEndPut: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **endTripBody** | [**EndTripBody**](EndTripBody.md)| Group body | 

### Return type

[**UpdateRecordsResponse**](UpdateRecordsResponse.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **groupApiTripStartPost**
> TripResponse groupApiTripStartPost(startTripBody)

Return newly created trip

Start a new trip

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BearerAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BearerAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BearerAuth').password = 'YOUR_PASSWORD';

var api_instance = TripApi();
var startTripBody = StartTripBody(); // StartTripBody | Group body

try { 
    var result = api_instance.groupApiTripStartPost(startTripBody);
    print(result);
} catch (e) {
    print("Exception when calling TripApi->groupApiTripStartPost: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **startTripBody** | [**StartTripBody**](StartTripBody.md)| Group body | 

### Return type

[**TripResponse**](TripResponse.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **groupApiTripsGroupsGroupIdGet**
> List<TripWithGroupAndDeviceAndUserResponse> groupApiTripsGroupsGroupIdGet(groupId)

Returns array of trips by groupId

Get trips by groupId

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BearerAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BearerAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BearerAuth').password = 'YOUR_PASSWORD';

var api_instance = TripApi();
var groupId = 56; // int | Group id

try { 
    var result = api_instance.groupApiTripsGroupsGroupIdGet(groupId);
    print(result);
} catch (e) {
    print("Exception when calling TripApi->groupApiTripsGroupsGroupIdGet: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **groupId** | **int**| Group id | [default to null]

### Return type

[**List<TripWithGroupAndDeviceAndUserResponse>**](TripWithGroupAndDeviceAndUserResponse.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

