# openapi.api.ChildApi

## Load the API package
```dart
import 'package:openapi/api.dart';
```

All URIs are relative to *https://production.presentation-api.api.findnemo.in*

Method | HTTP request | Description
------------- | ------------- | -------------
[**userApiChildIdDelete**](ChildApi.md#userApiChildIdDelete) | **DELETE** /user-api/child/{id} | Returns number of child deleted
[**userApiChildPost**](ChildApi.md#userApiChildPost) | **POST** /user-api/child/ | Returns newly created child
[**userApiChildPut**](ChildApi.md#userApiChildPut) | **PUT** /user-api/child/ | Returns number of child updated
[**userApiChildsGet**](ChildApi.md#userApiChildsGet) | **GET** /user-api/childs/ | Returns an array of child object by phone number


# **userApiChildIdDelete**
> DeleteRecordsResponse userApiChildIdDelete(id)

Returns number of child deleted

Delete a child by id

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BearerAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BearerAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BearerAuth').password = 'YOUR_PASSWORD';

var api_instance = ChildApi();
var id = 8.14; // num | Child id

try { 
    var result = api_instance.userApiChildIdDelete(id);
    print(result);
} catch (e) {
    print("Exception when calling ChildApi->userApiChildIdDelete: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **num**| Child id | [default to null]

### Return type

[**DeleteRecordsResponse**](DeleteRecordsResponse.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **userApiChildPost**
> ChildResponse userApiChildPost(createChildBody)

Returns newly created child

Create a child for a parent

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BearerAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BearerAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BearerAuth').password = 'YOUR_PASSWORD';

var api_instance = ChildApi();
var createChildBody = CreateChildBody(); // CreateChildBody | Child body

try { 
    var result = api_instance.userApiChildPost(createChildBody);
    print(result);
} catch (e) {
    print("Exception when calling ChildApi->userApiChildPost: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **createChildBody** | [**CreateChildBody**](CreateChildBody.md)| Child body | 

### Return type

[**ChildResponse**](ChildResponse.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **userApiChildPut**
> UpdateRecordsResponse userApiChildPut(updateChildBody)

Returns number of child updated

Update a child for a parent

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BearerAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BearerAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BearerAuth').password = 'YOUR_PASSWORD';

var api_instance = ChildApi();
var updateChildBody = UpdateChildBody(); // UpdateChildBody | Child body

try { 
    var result = api_instance.userApiChildPut(updateChildBody);
    print(result);
} catch (e) {
    print("Exception when calling ChildApi->userApiChildPut: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **updateChildBody** | [**UpdateChildBody**](UpdateChildBody.md)| Child body | 

### Return type

[**UpdateRecordsResponse**](UpdateRecordsResponse.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **userApiChildsGet**
> List<ChildResponse> userApiChildsGet()

Returns an array of child object by phone number

Get all the childs by phone number

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BearerAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BearerAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BearerAuth').password = 'YOUR_PASSWORD';

var api_instance = ChildApi();

try { 
    var result = api_instance.userApiChildsGet();
    print(result);
} catch (e) {
    print("Exception when calling ChildApi->userApiChildsGet: $e\n");
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**List<ChildResponse>**](ChildResponse.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

