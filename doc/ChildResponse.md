# openapi.model.ChildResponse

## Load the model package
```dart
import 'package:openapi/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **num** |  | [default to null]
**fullName** | **String** |  | [default to null]
**parentId** | **num** |  | [default to null]
**addressId** | **num** |  | [optional] [default to null]
**createdAt** | **String** |  | [default to null]
**updatedAt** | **String** |  | [default to null]
**parent** | [**UserResponse**](UserResponse.md) |  | [default to null]
**address** | [**AddressResponse**](AddressResponse.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


