# openapi.model.ErrorResponse

## Load the model package
```dart
import 'package:openapi/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**requestId** | **String** |  | [default to null]
**timestamp** | **num** |  | [default to null]
**status** | **num** |  | [default to null]
**message** | **String** |  | [default to null]
**source_** | **String** |  | [default to null]
**stack** | **String** |  | [optional] [default to null]
**code** | **String** |  | [default to null]
**errorData** | [**Object**](.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


