part of openapi.api;



class TripApi {
  final ApiClient apiClient;

  TripApi([ApiClient apiClient]) : apiClient = apiClient ?? defaultApiClient;

  /// Returns array of latest trip for each child by user with HTTP info returned
  ///
  /// Get latest trips for each child by user
  Future<Response> groupApiChildTripsLatestGetWithHttpInfo() async {
    Object postBody;

    // verify required params are set

    // create path and map variables
    String path = "/group-api/child-trips/latest/".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String contentType = contentTypes.isNotEmpty ? contentTypes[0] : "application/json";
    List<String> authNames = ["BearerAuth"];

    if(contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'GET',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             contentType,
                                             authNames);
    return response;
  }

  /// Returns array of latest trip for each child by user
  ///
  /// Get latest trips for each child by user
  Future<List<TripWithGroupAndDeviceAndUserResponse>> groupApiChildTripsLatestGet() async {
    Response response = await groupApiChildTripsLatestGetWithHttpInfo();
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return (apiClient.deserialize(_decodeBodyBytes(response), 'List<TripWithGroupAndDeviceAndUserResponse>') as List).map((item) => item as TripWithGroupAndDeviceAndUserResponse).toList();
    } else {
      return null;
    }
  }

  /// Return number of trips ended with HTTP info returned
  ///
  /// End a trip
  Future<Response> groupApiTripEndPutWithHttpInfo(EndTripBody endTripBody) async {
    Object postBody = endTripBody;

    // verify required params are set
    if(endTripBody == null) {
     throw ApiException(400, "Missing required param: endTripBody");
    }

    // create path and map variables
    String path = "/group-api/trip/end/".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String contentType = contentTypes.isNotEmpty ? contentTypes[0] : "application/json";
    List<String> authNames = ["BearerAuth"];

    if(contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'PUT',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             contentType,
                                             authNames);
    return response;
  }

  /// Return number of trips ended
  ///
  /// End a trip
  Future<UpdateRecordsResponse> groupApiTripEndPut(EndTripBody endTripBody) async {
    Response response = await groupApiTripEndPutWithHttpInfo(endTripBody);
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'UpdateRecordsResponse') as UpdateRecordsResponse;
    } else {
      return null;
    }
  }

  /// Return newly created trip with HTTP info returned
  ///
  /// Start a new trip
  Future<Response> groupApiTripStartPostWithHttpInfo(StartTripBody startTripBody) async {
    Object postBody = startTripBody;

    // verify required params are set
    if(startTripBody == null) {
     throw ApiException(400, "Missing required param: startTripBody");
    }

    // create path and map variables
    String path = "/group-api/trip/start/".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String contentType = contentTypes.isNotEmpty ? contentTypes[0] : "application/json";
    List<String> authNames = ["BearerAuth"];

    if(contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             contentType,
                                             authNames);
    return response;
  }

  /// Return newly created trip
  ///
  /// Start a new trip
  Future<TripResponse> groupApiTripStartPost(StartTripBody startTripBody) async {
    Response response = await groupApiTripStartPostWithHttpInfo(startTripBody);
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'TripResponse') as TripResponse;
    } else {
      return null;
    }
  }

  /// Returns array of trips by groupId with HTTP info returned
  ///
  /// Get trips by groupId
  Future<Response> groupApiTripsGroupsGroupIdGetWithHttpInfo(int groupId) async {
    Object postBody;

    // verify required params are set
    if(groupId == null) {
     throw ApiException(400, "Missing required param: groupId");
    }

    // create path and map variables
    String path = "/group-api/trips/groups/{groupId}/".replaceAll("{format}","json").replaceAll("{" + "groupId" + "}", groupId.toString());

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String contentType = contentTypes.isNotEmpty ? contentTypes[0] : "application/json";
    List<String> authNames = ["BearerAuth"];

    if(contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'GET',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             contentType,
                                             authNames);
    return response;
  }

  /// Returns array of trips by groupId
  ///
  /// Get trips by groupId
  Future<List<TripWithGroupAndDeviceAndUserResponse>> groupApiTripsGroupsGroupIdGet(int groupId) async {
    Response response = await groupApiTripsGroupsGroupIdGetWithHttpInfo(groupId);
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return (apiClient.deserialize(_decodeBodyBytes(response), 'List<TripWithGroupAndDeviceAndUserResponse>') as List).map((item) => item as TripWithGroupAndDeviceAndUserResponse).toList();
    } else {
      return null;
    }
  }

}
