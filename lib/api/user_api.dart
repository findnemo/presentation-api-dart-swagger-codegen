part of openapi.api;



class UserApi {
  final ApiClient apiClient;

  UserApi([ApiClient apiClient]) : apiClient = apiClient ?? defaultApiClient;

  /// Returns number of user deleted by phone number with HTTP info returned
  ///
  /// Delete the user by phone number
  Future<Response> userApiUserDeleteWithHttpInfo() async {
    Object postBody;

    // verify required params are set

    // create path and map variables
    String path = "/user-api/user/".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String contentType = contentTypes.isNotEmpty ? contentTypes[0] : "application/json";
    List<String> authNames = ["BearerAuth"];

    if(contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'DELETE',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             contentType,
                                             authNames);
    return response;
  }

  /// Returns number of user deleted by phone number
  ///
  /// Delete the user by phone number
  Future<DeleteRecordsResponse> userApiUserDelete() async {
    Response response = await userApiUserDeleteWithHttpInfo();
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'DeleteRecordsResponse') as DeleteRecordsResponse;
    } else {
      return null;
    }
  }

  /// Returns user object by phone number with HTTP info returned
  ///
  /// Get the user by phone number
  Future<Response> userApiUserGetWithHttpInfo() async {
    Object postBody;

    // verify required params are set

    // create path and map variables
    String path = "/user-api/user/".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String contentType = contentTypes.isNotEmpty ? contentTypes[0] : "application/json";
    List<String> authNames = ["BearerAuth"];

    if(contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'GET',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             contentType,
                                             authNames);
    return response;
  }

  /// Returns user object by phone number
  ///
  /// Get the user by phone number
  Future<UserWithAddressResponse> userApiUserGet() async {
    Response response = await userApiUserGetWithHttpInfo();
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'UserWithAddressResponse') as UserWithAddressResponse;
    } else {
      return null;
    }
  }

  /// Returns user object with HTTP info returned
  ///
  /// Create a user as driver and/or parent
  Future<Response> userApiUserPostWithHttpInfo(CreateUserWithAddressBody createUserWithAddressBody) async {
    Object postBody = createUserWithAddressBody;

    // verify required params are set
    if(createUserWithAddressBody == null) {
     throw ApiException(400, "Missing required param: createUserWithAddressBody");
    }

    // create path and map variables
    String path = "/user-api/user/".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String contentType = contentTypes.isNotEmpty ? contentTypes[0] : "application/json";
    List<String> authNames = ["BearerAuth"];

    if(contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             contentType,
                                             authNames);
    return response;
  }

  /// Returns user object
  ///
  /// Create a user as driver and/or parent
  Future<UserResponse> userApiUserPost(CreateUserWithAddressBody createUserWithAddressBody) async {
    Response response = await userApiUserPostWithHttpInfo(createUserWithAddressBody);
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'UserResponse') as UserResponse;
    } else {
      return null;
    }
  }

  /// Returns number of user updated with HTTP info returned
  ///
  /// Update a user as driver and/or parent
  Future<Response> userApiUserPutWithHttpInfo(UpdateUserWithAddressBody updateUserWithAddressBody) async {
    Object postBody = updateUserWithAddressBody;

    // verify required params are set
    if(updateUserWithAddressBody == null) {
     throw ApiException(400, "Missing required param: updateUserWithAddressBody");
    }

    // create path and map variables
    String path = "/user-api/user/".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String contentType = contentTypes.isNotEmpty ? contentTypes[0] : "application/json";
    List<String> authNames = ["BearerAuth"];

    if(contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'PUT',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             contentType,
                                             authNames);
    return response;
  }

  /// Returns number of user updated
  ///
  /// Update a user as driver and/or parent
  Future<UpdateRecordsResponse> userApiUserPut(UpdateUserWithAddressBody updateUserWithAddressBody) async {
    Response response = await userApiUserPutWithHttpInfo(updateUserWithAddressBody);
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'UpdateRecordsResponse') as UpdateRecordsResponse;
    } else {
      return null;
    }
  }

}
