part of openapi.api;



class GroupApi {
  final ApiClient apiClient;

  GroupApi([ApiClient apiClient]) : apiClient = apiClient ?? defaultApiClient;

  /// Returns number of groups deleted with HTTP info returned
  ///
  /// Delete group
  Future<Response> groupApiGroupIdDeleteWithHttpInfo(int id) async {
    Object postBody;

    // verify required params are set
    if(id == null) {
     throw ApiException(400, "Missing required param: id");
    }

    // create path and map variables
    String path = "/group-api/group/{id}/".replaceAll("{format}","json").replaceAll("{" + "id" + "}", id.toString());

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String contentType = contentTypes.isNotEmpty ? contentTypes[0] : "application/json";
    List<String> authNames = ["BearerAuth"];

    if(contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'DELETE',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             contentType,
                                             authNames);
    return response;
  }

  /// Returns number of groups deleted
  ///
  /// Delete group
  Future<DeleteRecordsResponse> groupApiGroupIdDelete(int id) async {
    Response response = await groupApiGroupIdDeleteWithHttpInfo(id);
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'DeleteRecordsResponse') as DeleteRecordsResponse;
    } else {
      return null;
    }
  }

  /// Returns newly created group with HTTP info returned
  ///
  /// Create group
  Future<Response> groupApiGroupPostWithHttpInfo(CreateGroupBody createGroupBody) async {
    Object postBody = createGroupBody;

    // verify required params are set
    if(createGroupBody == null) {
     throw ApiException(400, "Missing required param: createGroupBody");
    }

    // create path and map variables
    String path = "/group-api/group/".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String contentType = contentTypes.isNotEmpty ? contentTypes[0] : "application/json";
    List<String> authNames = ["BearerAuth"];

    if(contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             contentType,
                                             authNames);
    return response;
  }

  /// Returns newly created group
  ///
  /// Create group
  Future<GroupResponse> groupApiGroupPost(CreateGroupBody createGroupBody) async {
    Response response = await groupApiGroupPostWithHttpInfo(createGroupBody);
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'GroupResponse') as GroupResponse;
    } else {
      return null;
    }
  }

  /// Returns updated group with HTTP info returned
  ///
  /// Update group
  Future<Response> groupApiGroupPutWithHttpInfo(UpdateGroupBody updateGroupBody) async {
    Object postBody = updateGroupBody;

    // verify required params are set
    if(updateGroupBody == null) {
     throw ApiException(400, "Missing required param: updateGroupBody");
    }

    // create path and map variables
    String path = "/group-api/group/".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String contentType = contentTypes.isNotEmpty ? contentTypes[0] : "application/json";
    List<String> authNames = ["BearerAuth"];

    if(contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'PUT',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             contentType,
                                             authNames);
    return response;
  }

  /// Returns updated group
  ///
  /// Update group
  Future<UpdateRecordsResponse> groupApiGroupPut(UpdateGroupBody updateGroupBody) async {
    Response response = await groupApiGroupPutWithHttpInfo(updateGroupBody);
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'UpdateRecordsResponse') as UpdateRecordsResponse;
    } else {
      return null;
    }
  }

}
