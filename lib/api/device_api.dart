part of openapi.api;



class DeviceApi {
  final ApiClient apiClient;

  DeviceApi([ApiClient apiClient]) : apiClient = apiClient ?? defaultApiClient;

  /// Returns device object with HTTP info returned
  ///
  /// Create a device for a user
  Future<Response> deviceApiDevicePostWithHttpInfo(CreateDeviceBody createDeviceBody) async {
    Object postBody = createDeviceBody;

    // verify required params are set
    if(createDeviceBody == null) {
     throw ApiException(400, "Missing required param: createDeviceBody");
    }

    // create path and map variables
    String path = "/device-api/device/".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String contentType = contentTypes.isNotEmpty ? contentTypes[0] : "application/json";
    List<String> authNames = ["BearerAuth"];

    if(contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             contentType,
                                             authNames);
    return response;
  }

  /// Returns device object
  ///
  /// Create a device for a user
  Future<DeviceResponse> deviceApiDevicePost(CreateDeviceBody createDeviceBody) async {
    Response response = await deviceApiDevicePostWithHttpInfo(createDeviceBody);
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'DeviceResponse') as DeviceResponse;
    } else {
      return null;
    }
  }

  /// Returns device object with HTTP info returned
  ///
  /// Get a device for a user by uId
  Future<Response> deviceApiDeviceUIdUIdGetWithHttpInfo(String uId) async {
    Object postBody;

    // verify required params are set
    if(uId == null) {
     throw ApiException(400, "Missing required param: uId");
    }

    // create path and map variables
    String path = "/device-api/device/uId/{uId}/".replaceAll("{format}","json").replaceAll("{" + "uId" + "}", uId.toString());

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String contentType = contentTypes.isNotEmpty ? contentTypes[0] : "application/json";
    List<String> authNames = ["BearerAuth"];

    if(contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'GET',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             contentType,
                                             authNames);
    return response;
  }

  /// Returns device object
  ///
  /// Get a device for a user by uId
  Future<DeviceWithUserResponse> deviceApiDeviceUIdUIdGet(String uId) async {
    Response response = await deviceApiDeviceUIdUIdGetWithHttpInfo(uId);
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'DeviceWithUserResponse') as DeviceWithUserResponse;
    } else {
      return null;
    }
  }

}
