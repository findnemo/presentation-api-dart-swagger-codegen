part of openapi.api;



class PickupDropoffApi {
  final ApiClient apiClient;

  PickupDropoffApi([ApiClient apiClient]) : apiClient = apiClient ?? defaultApiClient;

  /// Returns array of pickup-dropoff by tripId with HTTP info returned
  ///
  /// Get pickup-dropoff by tripId
  Future<Response> groupApiPickupDropoffsTripsTripIdGetWithHttpInfo(int tripId) async {
    Object postBody;

    // verify required params are set
    if(tripId == null) {
     throw ApiException(400, "Missing required param: tripId");
    }

    // create path and map variables
    String path = "/group-api/pickup-dropoffs/trips/{tripId}/".replaceAll("{format}","json").replaceAll("{" + "tripId" + "}", tripId.toString());

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String contentType = contentTypes.isNotEmpty ? contentTypes[0] : "application/json";
    List<String> authNames = ["BearerAuth"];

    if(contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'GET',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             contentType,
                                             authNames);
    return response;
  }

  /// Returns array of pickup-dropoff by tripId
  ///
  /// Get pickup-dropoff by tripId
  Future<List<PickupDropoffWithTripAndChildAndDriverResponse>> groupApiPickupDropoffsTripsTripIdGet(int tripId) async {
    Response response = await groupApiPickupDropoffsTripsTripIdGetWithHttpInfo(tripId);
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return (apiClient.deserialize(_decodeBodyBytes(response), 'List<PickupDropoffWithTripAndChildAndDriverResponse>') as List).map((item) => item as PickupDropoffWithTripAndChildAndDriverResponse).toList();
    } else {
      return null;
    }
  }

  /// Returns number of children Dropoff with HTTP info returned
  ///
  /// Dropoff childrens by tripId
  Future<Response> groupApiTripsDropoffPutWithHttpInfo(CreateDropoffBody createDropoffBody) async {
    Object postBody = createDropoffBody;

    // verify required params are set
    if(createDropoffBody == null) {
     throw ApiException(400, "Missing required param: createDropoffBody");
    }

    // create path and map variables
    String path = "/group-api/trips/dropoff/".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String contentType = contentTypes.isNotEmpty ? contentTypes[0] : "application/json";
    List<String> authNames = ["BearerAuth"];

    if(contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'PUT',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             contentType,
                                             authNames);
    return response;
  }

  /// Returns number of children Dropoff
  ///
  /// Dropoff childrens by tripId
  Future<UpdateRecordsResponse> groupApiTripsDropoffPut(CreateDropoffBody createDropoffBody) async {
    Response response = await groupApiTripsDropoffPutWithHttpInfo(createDropoffBody);
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'UpdateRecordsResponse') as UpdateRecordsResponse;
    } else {
      return null;
    }
  }

  /// Returns array of pickupdropoff with HTTP info returned
  ///
  /// Pickup childrens by tripId
  Future<Response> groupApiTripsPickupPostWithHttpInfo(CreatePickupBody createPickupBody) async {
    Object postBody = createPickupBody;

    // verify required params are set
    if(createPickupBody == null) {
     throw ApiException(400, "Missing required param: createPickupBody");
    }

    // create path and map variables
    String path = "/group-api/trips/pickup/".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String contentType = contentTypes.isNotEmpty ? contentTypes[0] : "application/json";
    List<String> authNames = ["BearerAuth"];

    if(contentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             contentType,
                                             authNames);
    return response;
  }

  /// Returns array of pickupdropoff
  ///
  /// Pickup childrens by tripId
  Future<List<PickupDropoffResponse>> groupApiTripsPickupPost(CreatePickupBody createPickupBody) async {
    Response response = await groupApiTripsPickupPostWithHttpInfo(createPickupBody);
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return (apiClient.deserialize(_decodeBodyBytes(response), 'List<PickupDropoffResponse>') as List).map((item) => item as PickupDropoffResponse).toList();
    } else {
      return null;
    }
  }

}
